package user;

import org.apache.commons.codec.digest.DigestUtils;

import javax.jws.soap.SOAPBinding;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {
        UserStorage userStorage = new UserStorage();

        User user = userStorage.getByUsername("Bogdan");

        System.out.println(user.getPassword());
    }
}
