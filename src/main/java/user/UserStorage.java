package user;

import java.io.*;
import java.sql.*;
import java.util.Scanner;


public class UserStorage {

    public void save(User user) throws Exception {
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/javaadvanced?user=root&password=");

        PreparedStatement statement = con.prepareStatement(
                "insert into users(username, password, email, gender) values (?, ?, ? ,?)"
        );

        statement.setString(1, user.getUserName());
        statement.setString(2, user.getPassword());
        statement.setString(3, user.getEmail());
        statement.setString(4, user.getGender());

        statement.executeUpdate();

        con.close();
    }

    public User getByUsername(String userName) throws SQLException {
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/javaadvanced?user=root&password=");

        PreparedStatement statement = con.prepareStatement("" +
                " select * from users where username = ?");
        statement.setString(1, userName);

        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            User user = new User();
            user.setUserName(resultSet.getString("username"));
            user.setPassword(resultSet.getString("password"));
            user.setEmail(resultSet.getString("email"));
            user.setGender(resultSet.getString("gender"));
            return user;
        }

        return null;
    }
}
